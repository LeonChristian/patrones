package identityMap;

import java.time.Instant;

public abstract class EntidadBase {
	
	private String id;
	
	private Instant creadoEn;
	
	private Instant actualizadoEn;
	
	
	
	public EntidadBase(String id) {
		this.id=id;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public Instant getCreadoEn() {
		return creadoEn;
	}
	
	public void setCreadoEn(Instant creadoEn) {
		this.creadoEn = creadoEn;
	}
	
	public Instant getActualizadoEn() {
		return actualizadoEn;
	}
	
	public void setActualizadoEn(Instant actualizadoEn) {
		this.actualizadoEn = actualizadoEn;
	}
	
	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
	
	

}
