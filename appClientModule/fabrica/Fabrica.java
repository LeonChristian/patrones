package fabrica;

public class Fabrica {
	
	public Fabrica(){}
	
	public IConexion obtenerConexionOracle(){
		return new ConexionOracle();
	}
	
	public IConexion obtenerConexionPosgre(){
		return new ConexionPostgre();
	}

}
