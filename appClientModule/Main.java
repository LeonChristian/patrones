import java.util.IdentityHashMap;
import java.util.Map;

import comando.Cerebro;
import comando.ComandoCaminar;
import comando.ComandoJugar;
import comando.IComando;
import comando.Persona;
import fabrica.Fabrica;
import fabrica.IConexion;
import identityMap.BaseDeDatos;
import identityMap.Usuario;
import singleton.Loggeador;

public class Main {
	public static void main(String[] args) {
		
		//Singleton
		Loggeador uno = Loggeador.getLoggeador();
		uno.muestraMensaje(String.valueOf(uno.getNumero()));
		
		Loggeador dos = Loggeador.getLoggeador();
		uno.setNumero(1993);
		dos.muestraMensaje(String.valueOf(dos.getNumero()));
		
		//Singleton
		
		
		//Comando
		Cerebro cerebroParaPersona=new Cerebro();
		
		IComando jugar = new ComandoJugar(cerebroParaPersona);
		
		IComando caminar =  new ComandoCaminar(cerebroParaPersona);
		
		Persona persona = new Persona(jugar,caminar);
		persona.vayaCaminar();
		persona.vayaJuegue();
		//Comando
		
		//Fabrica
		Fabrica fabrica = new Fabrica();
		IConexion conexion = fabrica.obtenerConexionOracle();
		conexion.mostrarTipoConexion();
		conexion = fabrica.obtenerConexionPosgre();
		conexion.mostrarTipoConexion();
		//Fabrica
		
		Map<Usuario,String> mapa = new IdentityHashMap<Usuario,String>();
		BaseDeDatos db = new BaseDeDatos();
		System.out.println(db.getById(String.valueOf(1)));
		mapa.put(db.getById(String.valueOf(1)),String.valueOf(1)); 
		System.out.println(mapa.get(db.getById(String.valueOf(1))).toString());
		
	}


	public Main() {
		super();
	}

}