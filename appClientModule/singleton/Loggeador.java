package singleton;

public class Loggeador {
	
	private static Loggeador loggeador;
	
	private int numero;
	
	private Loggeador() {
	}

	public static Loggeador getLoggeador(){
		
		if (loggeador == null){
			loggeador =  new Loggeador();
		}
		return loggeador;
	};
	
	public void setNumero(int n){
		
		numero = n;
		
	}
	
	public int getNumero(){
		return numero;
	}
	
	public void muestraMensaje(String m){
		System.out.println(m);
	}
	
	
}
