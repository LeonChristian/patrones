package comando;

public class ComandoJugar implements IComando {
	
	Cerebro cerebro;

	public ComandoJugar(Cerebro cerebro) {
		this.cerebro = cerebro;
	}

	@Override
	public void execute() {
		cerebro.jugar();

	}

}
