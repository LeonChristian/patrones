package comando;

public class ComandoCaminar implements IComando{

	Cerebro cerebro;
	
	
	public ComandoCaminar(Cerebro cerebro) {
		this.cerebro = cerebro;
	}


	@Override
	public void execute() {
		cerebro.caminar();
		
	}

}
