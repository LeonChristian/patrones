package identityMap;

import java.util.LinkedList;
import java.util.List;

public class BaseDeDatos {
	private int llamadas;
	
	private List<Usuario> usuarios= new LinkedList<Usuario>();

	public BaseDeDatos() {
		Usuario u;
        for(int i=1; i<10; i++){
        	u = new Usuario(String.valueOf(i),"nombre"+i,"segundonombre"+i,"apellido"+i,"segundoapellido"+i);
            this.usuarios.add(u);
       }
	}

	public int getLlamadas() {
		return llamadas;
	}


	public List<Usuario> getUsuarios() {
		llamadas++;
		return usuarios;
	}	
	
	public Usuario getById(String id){
		llamadas++;
		Usuario s = null;
		for (Usuario u : usuarios){
			if (u.getId().equals(id))
				s = u;
		}
		return s;
	}
	
	public void crearUsuario(Usuario usuario){
		llamadas++;
		this.usuarios.add(usuario);
	}
	

}
