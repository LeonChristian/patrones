package identityMap;

import java.util.Date;

public class Usuario extends EntidadBase{
	
	private String nombre;
	
	private String segundonombre;

	private String apellido;
	
	private String segundoApellido;
	
	private Date fechaNac;
	
	

	/**
	 * @param nombre
	 * @param segundonombre
	 * @param apellido
	 * @param segundoApellido
	 * @param fechaNac
	 */
	public Usuario(String id,String nombre, String segundonombre, String apellido, String segundoApellido) {
		super(id);
		this.nombre = nombre;
		this.segundonombre = segundonombre;
		this.apellido = apellido;
		this.segundoApellido = segundoApellido;
		this.fechaNac = new Date();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSegundonombre() {
		return segundonombre;
	}

	public void setSegundonombre(String segundonombre) {
		this.segundonombre = segundonombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public Date getFechaNac() {
		return fechaNac;
	}

	public void setFechaNac(Date fechaNac) {
		this.fechaNac = fechaNac;
	}
	
	 @Override
	public String toString() {
		 return "Usuario:[nombre:"+nombre+",id:"+this.getId()+",fechanac:"+fechaNac+"]";
	 }
}
