package comando;

public class Persona {
	
	IComando jugar, caminar;
	
	public Persona(IComando jugar, IComando caminar){
		this.jugar=jugar;
		this.caminar=caminar;
	}
	
	public void vayaJuegue(){
		
		jugar.execute();
		
	}
	
	public void vayaCaminar(){
		caminar.execute();
	}

}
